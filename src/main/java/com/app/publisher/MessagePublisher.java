package com.app.publisher;

import com.app.model.dto.MessageDTO;
import com.app.model.entity.enumeration.Action;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@EnableScheduling
public class MessagePublisher {

    private static Logger logger = LoggerFactory.getLogger(MessagePublisher.class);

    private final String URL = "http://localhost:8080";

    private ConcurrentLinkedQueue<MessageDTO> messagesQueue = new ConcurrentLinkedQueue<>();
    private Integer lastMessageId = 1;

    MessagePublisher() {
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(5);
        executorService.scheduleAtFixedRate(this::handleMessages, 15, 15, TimeUnit.SECONDS);
        executorService.scheduleAtFixedRate(this::handleMessages, 15, 15, TimeUnit.SECONDS);
        executorService.scheduleAtFixedRate(this::handleMessages, 15, 15, TimeUnit.SECONDS);
        executorService.scheduleAtFixedRate(this::handleMessages, 15, 15, TimeUnit.SECONDS);
        executorService.scheduleAtFixedRate(this::handleMessages, 15, 15, TimeUnit.SECONDS);
    }

    public void handleMessages() {
        logger.info("Handling queue");
        if (messagesQueue.isEmpty()) {
            return;
        }
        MessageDTO message = messagesQueue.poll();
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> requestEntity = new HttpEntity<>(message, headers);
        logger.info(String.format("Sending message with id %d and action %s at %s", message.getId(), message.getAction(), Instant.now()));
        restTemplate.postForObject(String.format("%s/api/message", URL), requestEntity, String.class);
    }

    @Scheduled(fixedRate = 3000)
    private void generateMessage() {
        MessageDTO message = new MessageDTO();
        message.setId(lastMessageId++);
        message.setMsisdn((long) (Math.random() * 10000000000L));
        short action = (short) (Math.random() * 2);
        if (action == 0) {
            message.setAction(Action.PURCHASE);
        } else {
            message.setAction(Action.SUBSCRIPTION);
        }
        message.setTimestamp(System.currentTimeMillis() / 1000L);
        messagesQueue.add(message);
        logger.info(String.format("Message created with id %d and action %s", message.getId(), message.getAction()));
    }
}
