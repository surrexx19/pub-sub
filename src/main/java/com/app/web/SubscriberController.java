package com.app.web;

import com.app.model.dto.MessageDTO;
import com.app.model.entity.enumeration.Action;
import com.app.model.mapper.PurchaseMapper;
import com.app.model.mapper.SubscriptionMapper;
import com.app.service.SubscriberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SubscriberController {

    private static Logger logger = LoggerFactory.getLogger(SubscriberController.class);

    private final SubscriberService subscriberService;
    private final PurchaseMapper purchaseMapper;
    private final SubscriptionMapper subscriptionMapper;

    private SubscriberController(SubscriberService subscriberService,
                                 PurchaseMapper purchaseMapper,
                                 SubscriptionMapper subscriptionMapper) {
        this.subscriberService = subscriberService;
        this.subscriptionMapper = subscriptionMapper;
        this.purchaseMapper = purchaseMapper;
    }

    @PostMapping("/message")
    public void message (@RequestBody MessageDTO messageDTO) {
        if (messageDTO.getAction().equals(Action.PURCHASE)) {
            logger.info(String.format("Message with id %d will be saved as purchase", messageDTO.getId()));
            this.subscriberService.savePurchase(purchaseMapper.messageDtoToPurchase(messageDTO));
        } else {
            logger.info(String.format("Message with id %d will be saved as subscription", messageDTO.getId()));
            this.subscriberService.saveSubscription(subscriptionMapper.messageDtoToSubscription(messageDTO));
        }
    }

}
