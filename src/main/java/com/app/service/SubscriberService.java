package com.app.service;

import com.app.dao.PurchaseRepository;
import com.app.dao.SubscriptionRepository;
import com.app.model.entity.Purchase;
import com.app.model.entity.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class SubscriberService {

    private static Logger logger = LoggerFactory.getLogger(SubscriberService.class);

    private final PurchaseRepository purchaseRepository;
    private final SubscriptionRepository subscriptionRepository;

    public SubscriberService(PurchaseRepository purchaseRepository, SubscriptionRepository subscriptionRepository) {
        this.purchaseRepository = purchaseRepository;
        this.subscriptionRepository = subscriptionRepository;
    }

    public void savePurchase(Purchase purchase) {
        this.purchaseRepository.save(purchase);
        logger.info(String.format("Purchase with id %d was saved at %s", purchase.getId(), Instant.now()));
    }

    public void saveSubscription(Subscription subscription) {
        this.subscriptionRepository.save(subscription);
        logger.info(String.format("Subscription with id %d was saved at %s", subscription.getId(), Instant.now()));
    }

}
