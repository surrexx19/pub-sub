package com.app.model.entity.enumeration;

public enum Action {
    PURCHASE,
    SUBSCRIPTION;
}
