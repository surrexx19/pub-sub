package com.app.model.mapper;

import com.app.model.dto.MessageDTO;
import com.app.model.entity.Subscription;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public interface SubscriptionMapper {

    Subscription messageDtoToSubscription(MessageDTO messageDTO);


}
