package com.app.model.mapper;

import com.app.model.dto.MessageDTO;
import com.app.model.entity.Purchase;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public interface PurchaseMapper {

    Purchase messageDtoToPurchase (MessageDTO messageDTO);

}
